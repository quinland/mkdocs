# mlxprs: MarkLogic developer tools for VS Code

Hosted on [GitLab Pages](https://pages.gitlab.io). This page's current primary purpose is to host assets for the Visual Studio Code extension page.

![mlxprs avatar](mlxprs-vscode.png)

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md           # This page
        mlxprs-vscode.png  # The logo
